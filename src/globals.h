#ifndef GLOBALS_H
#define GLOBALS_H

#include "defs.h"

extern int *g_canvasGlyphs;
extern Color *g_canvasColors;

extern int g_canvasPixelWidth;
extern int g_canvasPixelHeight;
extern int g_canvasGlyphWidth;
extern int g_canvasGlyphHeight;

#endif
