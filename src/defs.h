#ifndef DEFS_H
#define DEFS_H

#include <stdio.h>
#include <stdlib.h>
#include <raylib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>

#define VERSION "V 1.0"
#define FILEFORMATVERSION "0"
#define M_PI 3.1415
#define SCREEN_WIDTH 900
#define SCREEN_HEIGHT 600 
#define GLYPHWIDTH 9 
#define GLYPHHEIGHT 16 

enum fileTypes { HTML = 0, SAPP, PNG };

#endif
