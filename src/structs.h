#ifndef STRUCTS_H
#define STRUCTS_H

struct int_array {
	int *array;
	int size;
};

struct char_array {
	char **array;
	int size;
};

struct int_and_char_array {
	struct int_array *intarray;
	struct char_array *chararray;
};

#endif
