#include "ui.h"
#include "defs.h"
#include "globals.h"
#include "canvas.h"
#define RAYGUI_IMPLEMENTATION
#define RAYGUI_SUPPORT_ICONS
#include "../raygui/raygui.h"
#undef RAYGUI_IMPLEMENTATION

#define margin 8
#define widgetHeight 17

#define glyphPerRow 20
#define totalGlyphs 255

enum states { PLACE_GLYPH = 0, REPLACE_GLYPH, REPLACE_COLOR };
enum states canvasState = PLACE_GLYPH;

static Texture2D font;
static Texture2D background;
static RenderTexture2D backgroundScaled;
static Color backgroundColor = BLANK;
static bool drawBackground = false;

static int yPosWidgets = 0;

static const int xPosCanvas = glyphPerRow * GLYPHWIDTH + glyphPerRow;
static Vector2 canvasPixelOffset = { 0, 0 };

static Color selectedColor = BLACK;
static int selectedGlyphRight = 0;
static int selectedGlyphLeft = 1;

void ui_run()
{
	ui_glyph_select();
	ui_color_select();
	ui_menu();
	ui_canvas();

	GuiLabel( (Rectangle) { 0, SCREEN_HEIGHT - widgetHeight, 20, widgetHeight }, VERSION );
}

void ui_open()
{
	font = LoadTexture( "437.png" );
}

void ui_close()
{
	UnloadTexture( font );
	canvas_del();
	ui_clear_background();
}

void ui_glyph_select()
{
	for( int i = 0; i < 255; i++ ) {
		int row = i / glyphPerRow;
		int column = i % glyphPerRow;
		const int xPos = column * GLYPHWIDTH + column;
		yPosWidgets = row * GLYPHHEIGHT + row;

		if( i == selectedGlyphLeft ) DrawRectangle( xPos - 1, yPosWidgets - 1, GLYPHWIDTH + 2, GLYPHHEIGHT + 2, RED );
		else if( i == selectedGlyphRight ) DrawRectangle( xPos - 1, yPosWidgets - 1, GLYPHWIDTH + 2, GLYPHHEIGHT + 2, BLUE );

		const int xMouse = GetMouseX();
		const int yMouse = GetMouseY();
		if( xPos <= xMouse && xPos + GLYPHWIDTH >= xMouse && yPosWidgets <= yMouse && yPosWidgets + GLYPHHEIGHT >= yMouse ) {
			if( IsMouseButtonPressed( MOUSE_LEFT_BUTTON ) ) selectedGlyphLeft = i;
			else if( IsMouseButtonPressed( MOUSE_RIGHT_BUTTON ) ) selectedGlyphRight = i;
			else DrawRectangle( xPos, yPosWidgets, GLYPHWIDTH, GLYPHHEIGHT, GREEN );
		}

		DrawTextureRec( font, ( Rectangle ) { i * GLYPHWIDTH, 0, GLYPHWIDTH, GLYPHHEIGHT }, ( Vector2 ) { xPos, yPosWidgets }, BLACK );
	}

	yPosWidgets += GLYPHHEIGHT;
}

void ui_color_select()
{
	const int colorPickerHeight = 150;
	const int colorPickerWidth = GLYPHWIDTH * ( glyphPerRow - 2 ) - margin;
	const int colorLabelWidth = 10;
	const int colorBoxWidth = 27;

	int xPos = margin;
	yPosWidgets += margin;

	selectedColor = GuiColorPicker( ( Rectangle ) { xPos, yPosWidgets, colorPickerWidth, colorPickerHeight }, "", selectedColor );
	yPosWidgets += colorPickerHeight + margin;
	xPos = 0;

	static bool rEditMode = false;
	GuiLabel( (Rectangle) { xPos, yPosWidgets, colorLabelWidth, widgetHeight }, "R:" );
	xPos += colorLabelWidth;

	int r = (int) selectedColor.r;
	if( GuiValueBox( (Rectangle) { xPos, yPosWidgets, colorBoxWidth, widgetHeight }, NULL, &r, 0, 255, rEditMode ) ) rEditMode = !rEditMode;
	if( rEditMode ) selectedColor.r = r;
	xPos += colorBoxWidth + margin;

	static bool gEditMode = false;
	GuiLabel( (Rectangle) { xPos, yPosWidgets, colorLabelWidth, widgetHeight }, "G:" );
	int g = (int) selectedColor.g;
	xPos += colorLabelWidth;

	if( GuiValueBox( (Rectangle) { xPos, yPosWidgets, colorBoxWidth, widgetHeight }, NULL, &g, 0, 255, gEditMode ) ) gEditMode = !gEditMode;
	if( gEditMode ) selectedColor.g = g;
	xPos += colorBoxWidth + margin;

	static bool bEditMode = false;
	GuiLabel( (Rectangle) { xPos, yPosWidgets, colorLabelWidth, widgetHeight }, "B:" );
	xPos += colorLabelWidth;

	int b = (int) selectedColor.b;
	if( GuiValueBox( (Rectangle) { xPos, yPosWidgets, colorBoxWidth, widgetHeight }, NULL, &b, 0, 255, bEditMode ) ) bEditMode = !bEditMode;
	if( bEditMode ) selectedColor.b = b;
	xPos += colorBoxWidth + margin;

	GuiLabel( (Rectangle) { xPos, yPosWidgets, 25, widgetHeight }, "Color:" );
	xPos += 25 + margin;

	DrawRectangle( xPos, yPosWidgets, widgetHeight, widgetHeight, selectedColor );

	yPosWidgets += widgetHeight;
}

void ui_menu()
{
	const int buttonNewWidth = 30;
	static int xNewGlyphSizeCanvas = 20;
	static int yNewGlyphSizeCanvas = 20;

	static bool xSizeEditMode = false;
	static bool ySizeEditMode = false;
	const int coordLabelWidth = 10;
	const int coordBoxWidth = 40;
	int xPos = 0;
	yPosWidgets += margin;

	GuiLabel( (Rectangle) { xPos, yPosWidgets, 50, widgetHeight }, "Replace:" );
	xPos += 50;
	if( GuiButton( (Rectangle) { xPos, yPosWidgets, 40, widgetHeight }, "Glyph" ) ) {
		if( canvasState != REPLACE_GLYPH ) canvasState = REPLACE_GLYPH;
		else canvasState = PLACE_GLYPH;
	}
	if( canvasState == REPLACE_GLYPH ) DrawRectangleLinesEx( (Rectangle) { xPos, yPosWidgets, 40, widgetHeight }, 3, GREEN );
	xPos += 40 + margin;
	if( GuiButton( (Rectangle) { xPos, yPosWidgets, 40, widgetHeight }, "Color" ) ) {
		if( canvasState != REPLACE_COLOR ) canvasState = REPLACE_COLOR;
		else canvasState = PLACE_GLYPH;
	}
	if( canvasState == REPLACE_COLOR ) DrawRectangleLinesEx( (Rectangle) { xPos, yPosWidgets, 40, widgetHeight }, 3, GREEN );
	xPos += 40 + margin;
	xPos = 0;
	yPosWidgets += widgetHeight + margin;

	GuiLabel( (Rectangle) { xPos, yPosWidgets, 90, widgetHeight }, "Background color:" );
	xPos += 90 + margin;
	if( GuiButton( (Rectangle) { xPos, yPosWidgets, 30, widgetHeight }, "Set" ) ) ui_set_background_color( selectedColor );
	xPos += 30 + margin;
	if( GuiButton( (Rectangle) { xPos, yPosWidgets, 38, widgetHeight }, "Clear" ) ) ui_clear_background_color();
	xPos = 0;
	yPosWidgets += widgetHeight + margin;

	if( GuiButton( (Rectangle) { xPos, yPosWidgets, buttonNewWidth, widgetHeight }, "New" ) ) canvas_new( xNewGlyphSizeCanvas, yNewGlyphSizeCanvas );
	xPos += buttonNewWidth + margin;
	GuiLabel( (Rectangle) { xPos, yPosWidgets, coordLabelWidth, widgetHeight }, "X:" );
	xPos += coordLabelWidth;
	if( GuiValueBox( (Rectangle) { xPos, yPosWidgets, coordBoxWidth, widgetHeight }, NULL, &xNewGlyphSizeCanvas, 0, 1000, xSizeEditMode ) ) xSizeEditMode = !xSizeEditMode;
	xPos += coordBoxWidth + margin;
	GuiLabel( (Rectangle) { xPos, yPosWidgets, coordLabelWidth, widgetHeight }, "Y:" );
	xPos += coordLabelWidth;
	if( GuiValueBox( (Rectangle) { xPos, yPosWidgets, coordBoxWidth, widgetHeight }, NULL, &yNewGlyphSizeCanvas, 0, 1000, ySizeEditMode ) ) ySizeEditMode = !ySizeEditMode;
	xPos = 0;
	yPosWidgets += widgetHeight + margin;


	if( GuiButton( (Rectangle) { xPos, yPosWidgets, 150, widgetHeight }, "Clear Background Image" ) ) ui_clear_background();
	yPosWidgets += widgetHeight + margin;

	if( GuiButton( (Rectangle) { xPos, yPosWidgets, 100, widgetHeight }, "Save" ) ) canvas_save();
	yPosWidgets += widgetHeight + margin;

	if( GuiButton( (Rectangle) { xPos, yPosWidgets, 100, widgetHeight }, "Export to HTML" ) ) canvas_export_html();
	yPosWidgets += widgetHeight + margin;

	if( GuiButton( (Rectangle) { xPos, yPosWidgets, 100, widgetHeight }, "Export to PNG" ) ) canvas_export_png();
}

void ui_canvas()
{
	Rectangle view = GuiScrollPanel( (Rectangle) { xPosCanvas, 0, SCREEN_WIDTH - xPosCanvas, SCREEN_HEIGHT }, "", (Rectangle){ xPosCanvas, 0, g_canvasPixelWidth, g_canvasPixelHeight }, &canvasPixelOffset );

	BeginScissorMode( view.x, view.y, view.width, view.height );

	if( drawBackground ) DrawTexture( backgroundScaled.texture, xPosCanvas + canvasPixelOffset.x, 0 + canvasPixelOffset.y, WHITE );
	else DrawRectangle( xPosCanvas, 0, SCREEN_WIDTH - xPosCanvas, SCREEN_HEIGHT, backgroundColor );

	DrawRectangleLines( xPosCanvas, 0, g_canvasPixelWidth + canvasPixelOffset.x, g_canvasPixelHeight + canvasPixelOffset.y, BLUE );

	ui_canvas_action();

	for( int x = 0; x < g_canvasGlyphWidth; x++ ) {
		for( int y = 0; y < g_canvasGlyphHeight; y++ ) {
			const int glyph = *( g_canvasGlyphs + x + y * g_canvasGlyphWidth );
			const Color color = *( g_canvasColors + x + y * g_canvasGlyphWidth );
			DrawTextureRec( font, ( Rectangle ) { glyph * GLYPHWIDTH, 0, GLYPHWIDTH, GLYPHHEIGHT }, ( Vector2 ) { xPosCanvas + x * GLYPHWIDTH + canvasPixelOffset.x, y * GLYPHHEIGHT + canvasPixelOffset.y }, color );
		}
	}

	EndScissorMode();
}

void ui_canvas_action()
{
	const int xMouseOnCanvas = GetMouseX() - xPosCanvas;
	if( xMouseOnCanvas < 0 ) return;

	const int xPosGlyph = ( xMouseOnCanvas - canvasPixelOffset.x ) / GLYPHWIDTH;
	if( xPosGlyph >= g_canvasGlyphWidth ) return;
	const int yPosGlyph = ( GetMouseY() - canvasPixelOffset.y ) / GLYPHHEIGHT;
	if( yPosGlyph >= g_canvasGlyphHeight ) return;

	DrawRectangle( xPosGlyph * GLYPHWIDTH + canvasPixelOffset.x + xPosCanvas, yPosGlyph * GLYPHHEIGHT + canvasPixelOffset.y, GLYPHWIDTH, GLYPHHEIGHT, GREEN );

	switch( canvasState ) {
		case PLACE_GLYPH:
			ui_place_glyph( xPosGlyph, yPosGlyph );
			break;
		case REPLACE_GLYPH:
			ui_replace_glyph( xPosGlyph, yPosGlyph );
			break;
		case REPLACE_COLOR:
			ui_replace_color( xPosGlyph, yPosGlyph );
			break;
		default:
			break;
	}
}

void ui_place_glyph( const int xPosGlyph, const int yPosGlyph )
{
	if( IsMouseButtonPressed( MOUSE_LEFT_BUTTON ) ) {
		*( g_canvasGlyphs + xPosGlyph + yPosGlyph * g_canvasGlyphWidth ) = selectedGlyphLeft;
		*( g_canvasColors + xPosGlyph + yPosGlyph * g_canvasGlyphWidth ) = selectedColor;
	}
	else if( IsMouseButtonPressed( MOUSE_RIGHT_BUTTON ) ) {
		*( g_canvasGlyphs + xPosGlyph + yPosGlyph * g_canvasGlyphWidth ) = selectedGlyphRight;
		*( g_canvasColors + xPosGlyph + yPosGlyph * g_canvasGlyphWidth ) = selectedColor;
	}
}

void ui_replace_glyph( const int xPosGlyph, const int yPosGlyph )
{
	int replacementGlyph;
	if( IsMouseButtonPressed( MOUSE_LEFT_BUTTON ) ) replacementGlyph = selectedGlyphLeft;
	else if( IsMouseButtonPressed( MOUSE_RIGHT_BUTTON ) ) replacementGlyph = selectedGlyphRight;
	else return;

	const int glyphToReplace = *( g_canvasGlyphs + xPosGlyph + yPosGlyph * g_canvasGlyphWidth );

	for( int i = 0; i < g_canvasGlyphWidth * g_canvasGlyphHeight; i++ ) {
		const int glyph = *( g_canvasGlyphs + i );
		if( glyph == glyphToReplace ) *( g_canvasGlyphs + i ) = replacementGlyph;
	}

	canvasState = PLACE_GLYPH;
}

void ui_replace_color( const int xPosGlyph, const int yPosGlyph )
{
	if( !IsMouseButtonPressed( MOUSE_LEFT_BUTTON ) ) return;

	const Color colorToReplace = *( g_canvasColors + xPosGlyph + yPosGlyph * g_canvasGlyphWidth );

	for( int i = 0; i < g_canvasGlyphWidth * g_canvasGlyphHeight; i++ ) {
		const Color color = *( g_canvasColors + i );
		if( color.r == colorToReplace.r && color.g == colorToReplace.g && color.b == colorToReplace.b ) *( g_canvasColors + i ) = selectedColor;
	}

	canvasState = PLACE_GLYPH;
}

void ui_set_background( char *filePath )
{
	ui_clear_background();

	drawBackground = true;

	background = LoadTexture( filePath );

	ui_scale_background();

}

void ui_scale_background()
{
	if( drawBackground ) UnloadRenderTexture( backgroundScaled );
	else return;

	backgroundScaled = LoadRenderTexture( g_canvasPixelWidth, g_canvasPixelHeight );

	BeginTextureMode( backgroundScaled );
	BeginDrawing();

	DrawTexturePro( background, ( Rectangle ) { 0, 0, background.width, -background.height }, ( Rectangle ) { 0, 0, g_canvasPixelWidth, g_canvasPixelHeight }, ( Vector2 ) { 0, 0 }, 0, WHITE );

	EndTextureMode();
}

void ui_clear_background()
{
	if( drawBackground ) {
		UnloadTexture( background );
		UnloadRenderTexture( backgroundScaled );
		drawBackground = false;
	}
}

void ui_clear_background_color()
{
	backgroundColor = BLANK;
}

Color ui_get_background_color()
{
	return backgroundColor;
}

void ui_set_background_color( Color bgColor )
{
	backgroundColor = bgColor;
}
