#ifndef CANVAS_H
#define CANVAS_H

#include "defs.h"

void canvas_open();
void canvas_close();
void canvas_new( const int, const int );
void canvas_del();
void canvas_check_dropped_file();
void canvas_save();
void canvas_load( const char* );
void canvas_export_html();
void canvas_export_png();
void canvas_append_string( char**, const char* );
const char* canvas_html_lookup( const int );
const char* canvas_get_filename( enum fileTypes );

#endif
