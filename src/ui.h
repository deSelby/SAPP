#ifndef UI_H
#define UI_H

#include "structs.h"
#include "defs.h"

void ui_run();
void ui_close();
void ui_open();
void ui_glyph_select();
void ui_color_select();
void ui_menu();
void ui_canvas();
void ui_set_background( char* );
void ui_scale_background();
void ui_clear_background();
void ui_clear_background_color();
void ui_canvas_action();
void ui_replace_color( const int, const int );
void ui_replace_glyph( const int, const int );
void ui_place_glyph( const int, const int );
Color ui_get_background_color();
void ui_set_background_color( Color );

#endif
