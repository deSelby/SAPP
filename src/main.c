#include "defs.h"
#include "ui.h"
#include "canvas.h"
#if defined( WASM )
#include <emscripten.h>
#endif

int *g_canvasGlyphs = NULL;
Color *g_canvasColors = NULL;

int g_canvasPixelWidth = 0;
int g_canvasPixelHeight = 0;
int g_canvasGlyphWidth = 0;
int g_canvasGlyphHeight = 0;

void program_loop();

int main( int argc, char *argv[] )
{
	if( argc > 1 && IsFileExtension( argv[1], ".sapp" ) ) canvas_load( argv[ 1 ] );
	else canvas_new( 20, 20 );

	InitWindow( SCREEN_WIDTH, SCREEN_HEIGHT, "SAPP" );

	ui_open();
	canvas_open();

	#if defined( WASM )
	emscripten_set_main_loop( program_loop, 30, 1 );
	#else
	SetTargetFPS( 30 );
	while( 1 ) {
		program_loop();
		if( WindowShouldClose() ) break;
	}
	#endif

	ui_close();
	canvas_close();
	CloseWindow();

	return 0;
}

void program_loop()
{
	canvas_check_dropped_file();
	BeginDrawing();
	ClearBackground( RAYWHITE );
	ui_run();
	EndDrawing();
}
