const char* unicode_lookup_table( const int index )
{
	switch( index ) {
		case 0:
			return "0020";
			break;
		case 1:
			return "263A";
			break;
		case 2:
			return "263B";
			break;
		case 3:
			return "2665";
			break;
		case 4:
			return "2666";
			break;
		case 5:
			return "2663";
			break;
		case 6:
			return "2660";
			break;
		case 7:
			return "2022";
			break;
		case 8:
			return "25D8";
			break;
		case 9:
			return "25CB";
			break;
		case 10:
			return "25D9";
			break;
		case 11:
			return "2642";
			break;
		case 12:
			return "2640";
			break;
		case 13:
			return "266A";
			break;
		case 14:
			return "266B";
			break;
		case 15:
			return "263C";
			break;
		case 16:
			return "25BA";
			break;
		case 17:
			return "25C4";
			break;
		case 18:
			return "2195";
			break;
		case 19:
			return "203C";
			break;
		case 20:
			return "00B6";
			break;
		case 21:
			return "00A7";
			break;
		case 22:
			return "25AC";
			break;
		case 23:
			return "21A8";
			break;
		case 24:
			return "2191";
			break;
		case 25:
			return "2193";
			break;
		case 26:
			return "2192";
			break;
		case 27:
			return "2190";
			break;
		case 28:
			return "221F";
			break;
		case 29:
			return "2194";
			break;
		case 30:
			return "25B2";
			break;
		case 31:
			return "25BC";
			break;
		case 32:
			return "0020";
			break;
		case 33:
			return "0021";
			break;
		case 34:
			return "0022";
			break;
		case 35:
			return "0023";
			break;
		case 36:
			return "0024";
			break;
		case 37:
			return "0025";
			break;
		case 38:
			return "0026";
			break;
		case 39:
			return "0027";
			break;
		case 40:
			return "0028";
			break;
		case 41:
			return "0029";
			break;
		case 42:
			return "002A";
			break;
		case 43:
			return "002B";
			break;
		case 44:
			return "002C";
			break;
		case 45:
			return "002D";
			break;
		case 46:
			return "002E";
			break;
		case 47:
			return "002F";
			break;
		case 48:
			return "0030";
			break;
		case 49:
			return "0031";
			break;
		case 50:
			return "0032";
			break;
		case 51:
			return "0033";
			break;
		case 52:
			return "0034";
			break;
		case 53:
			return "0035";
			break;
		case 54:
			return "0036";
			break;
		case 55:
			return "0037";
			break;
		case 56:
			return "0038";
			break;
		case 57:
			return "0039";
			break;
		case 58:
			return "003A";
			break;
		case 59:
			return "003B";
			break;
		case 60:
			return "003C";
			break;
		case 61:
			return "003D";
			break;
		case 62:
			return "003E";
			break;
		case 63:
			return "003F";
			break;
		case 64:
			return "0040";
			break;
		case 65:
			return "0041";
			break;
		case 66:
			return "0042";
			break;
		case 67:
			return "0043";
			break;
		case 68:
			return "0044";
			break;
		case 69:
			return "0045";
			break;
		case 70:
			return "0046";
			break;
		case 71:
			return "0047";
			break;
		case 72:
			return "0048";
			break;
		case 73:
			return "0049";
			break;
		case 74:
			return "004A";
			break;
		case 75:
			return "004B";
			break;
		case 76:
			return "004C";
			break;
		case 77:
			return "004D";
			break;
		case 78:
			return "004E";
			break;
		case 79:
			return "004F";
			break;
		case 80:
			return "0050";
			break;
		case 81:
			return "0051";
			break;
		case 82:
			return "0052";
			break;
		case 83:
			return "0053";
			break;
		case 84:
			return "0054";
			break;
		case 85:
			return "0055";
			break;
		case 86:
			return "0056";
			break;
		case 87:
			return "0057";
			break;
		case 88:
			return "0058";
			break;
		case 89:
			return "0059";
			break;
		case 90:
			return "005A";
			break;
		case 91:
			return "005B";
			break;
		case 92:
			return "005C";
			break;
		case 93:
			return "005D";
			break;
		case 94:
			return "005E";
			break;
		case 95:
			return "005F";
			break;
		case 96:
			return "0060";
			break;
		case 97:
			return "0061";
			break;
		case 98:
			return "0062";
			break;
		case 99:
			return "0063";
			break;
		case 100:
			return "0064";
			break;
		case 101:
			return "0065";
			break;
		case 102:
			return "0066";
			break;
		case 103:
			return "0067";
			break;
		case 104:
			return "0068";
			break;
		case 105:
			return "0069";
			break;
		case 106:
			return "006A";
			break;
		case 107:
			return "006B";
			break;
		case 108:
			return "006C";
			break;
		case 109:
			return "006D";
			break;
		case 110:
			return "006E";
			break;
		case 111:
			return "006F";
			break;
		case 112:
			return "0070";
			break;
		case 113:
			return "0071";
			break;
		case 114:
			return "0072";
			break;
		case 115:
			return "0073";
			break;
		case 116:
			return "0074";
			break;
		case 117:
			return "0075";
			break;
		case 118:
			return "0076";
			break;
		case 119:
			return "0077";
			break;
		case 120:
			return "0078";
			break;
		case 121:
			return "0079";
			break;
		case 122:
			return "007A";
			break;
		case 123:
			return "007B";
			break;
		case 124:
			return "007C";
			break;
		case 125:
			return "007D";
			break;
		case 126:
			return "007E";
			break;
		case 127:
			return "2302";
			break;
		case 128:
			return "00C7";
			break;
		case 129:
			return "00FC";
			break;
		case 130:
			return "00E9";
			break;
		case 131:
			return "00E2";
			break;
		case 132:
			return "00E4";
			break;
		case 133:
			return "00E0";
			break;
		case 134:
			return "00E5";
			break;
		case 135:
			return "00E7";
			break;
		case 136:
			return "00EA";
			break;
		case 137:
			return "00EB";
			break;
		case 138:
			return "00E8";
			break;
		case 139:
			return "00EF";
			break;
		case 140:
			return "00EE";
			break;
		case 141:
			return "00EC";
			break;
		case 142:
			return "00C4";
			break;
		case 143:
			return "00C5";
			break;
		case 144:
			return "00C9";
			break;
		case 145:
			return "00E6";
			break;
		case 146:
			return "00C6";
			break;
		case 147:
			return "00F4";
			break;
		case 148:
			return "00F6";
			break;
		case 149:
			return "00F2";
			break;
		case 150:
			return "00FB";
			break;
		case 151:
			return "00F9";
			break;
		case 152:
			return "00FF";
			break;
		case 153:
			return "00D6";
			break;
		case 154:
			return "00DC";
			break;
		case 155:
			return "00A2";
			break;
		case 156:
			return "00A3";
			break;
		case 157:
			return "00A5";
			break;
		case 158:
			return "20A7";
			break;
		case 159:
			return "0192";
			break;
		case 160:
			return "00E1";
			break;
		case 161:
			return "00ED";
			break;
		case 162:
			return "00F3";
			break;
		case 163:
			return "00FA";
			break;
		case 164:
			return "00F1";
			break;
		case 165:
			return "00D1";
			break;
		case 166:
			return "00AA";
			break;
		case 167:
			return "00BA";
			break;
		case 168:
			return "00BF";
			break;
		case 169:
			return "2310";
			break;
		case 170:
			return "00AC";
			break;
		case 171:
			return "00BD";
			break;
		case 172:
			return "00BC";
			break;
		case 173:
			return "00A1";
			break;
		case 174:
			return "00AB";
			break;
		case 175:
			return "00BB";
			break;
		case 176:
			return "2591";
			break;
		case 177:
			return "2592";
			break;
		case 178:
			return "2593";
			break;
		case 179:
			return "2502";
			break;
		case 180:
			return "2524";
			break;
		case 181:
			return "2561";
			break;
		case 182:
			return "2562";
			break;
		case 183:
			return "2556";
			break;
		case 184:
			return "2555";
			break;
		case 185:
			return "2563";
			break;
		case 186:
			return "2551";
			break;
		case 187:
			return "2557";
			break;
		case 188:
			return "255D";
			break;
		case 189:
			return "255C";
			break;
		case 190:
			return "255B";
			break;
		case 191:
			return "2510";
			break;
		case 192:
			return "2514";
			break;
		case 193:
			return "2534";
			break;
		case 194:
			return "252C";
			break;
		case 195:
			return "251C";
			break;
		case 196:
			return "2500";
			break;
		case 197:
			return "253C";
			break;
		case 198:
			return "255E";
			break;
		case 199:
			return "255F";
			break;
		case 200:
			return "255A";
			break;
		case 201:
			return "2554";
			break;
		case 202:
			return "2569";
			break;
		case 203:
			return "2566";
			break;
		case 204:
			return "2560";
			break;
		case 205:
			return "2550";
			break;
		case 206:
			return "256C";
			break;
		case 207:
			return "2567";
			break;
		case 208:
			return "2568";
			break;
		case 209:
			return "2564";
			break;
		case 210:
			return "2565";
			break;
		case 211:
			return "2559";
			break;
		case 212:
			return "2558";
			break;
		case 213:
			return "2552";
			break;
		case 214:
			return "2553";
			break;
		case 215:
			return "256B";
			break;
		case 216:
			return "256A";
			break;
		case 217:
			return "2518";
			break;
		case 218:
			return "250C";
			break;
		case 219:
			return "2588";
			break;
		case 220:
			return "2584";
			break;
		case 221:
			return "258C";
			break;
		case 222:
			return "2590";
			break;
		case 223:
			return "2580";
			break;
		case 224:
			return "03B1";
			break;
		case 225:
			return "00DF";
			break;
		case 226:
			return "0393";
			break;
		case 227:
			return "03C0";
			break;
		case 228:
			return "03A3";
			break;
		case 229:
			return "03C3";
			break;
		case 230:
			return "00B5";
			break;
		case 231:
			return "03C4";
			break;
		case 232:
			return "03A6";
			break;
		case 233:
			return "0398";
			break;
		case 234:
			return "03A9";
			break;
		case 235:
			return "03B4";
			break;
		case 236:
			return "221E";
			break;
		case 237:
			return "03C6";
			break;
		case 238:
			return "03B5";
			break;
		case 239:
			return "2229";
			break;
		case 240:
			return "2261";
			break;
		case 241:
			return "00B1";
			break;
		case 242:
			return "2265";
			break;
		case 243:
			return "2264";
			break;
		case 244:
			return "2320";
			break;
		case 245:
			return "2321";
			break;
		case 246:
			return "00F7";
			break;
		case 247:
			return "2248";
			break;
		case 248:
			return "00B0";
			break;
		case 249:
			return "2219";
			break;
		case 250:
			return "00B7";
			break;
		case 251:
			return "221A";
			break;
		case 252:
			return "207F";
			break;
		case 253:
			return "00B2";
			break;
		case 254:
			return "25A0";
			break;
		case 255:
			return "00A0";
			break;
		default:
			return "";
			break;
	}
}
