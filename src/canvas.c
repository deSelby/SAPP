#include "canvas.h"
#include "ui.h"
#include "unicode.h"
#include "globals.h"
#if defined( WASM )
#include <emscripten.h>
#endif

static Image font;

void canvas_open()
{
	font = LoadImage( "437.png" );
}

void canvas_close()
{
	UnloadImage( font );
}

void canvas_check_dropped_file()
{
	if( IsFileDropped() ) {
		FilePathList droppedFiles = LoadDroppedFiles();

		if( IsFileExtension( droppedFiles.paths[ 0 ], ".png" ) ) ui_set_background( droppedFiles.paths[ 0 ] );
		else if( IsFileExtension( droppedFiles.paths[ 0 ], ".sapp" ) ) canvas_load( droppedFiles.paths[ 0 ] );

		UnloadDroppedFiles( droppedFiles );
	}
}

void canvas_new( const int xSize, const int ySize )
{
	g_canvasGlyphs = realloc( g_canvasGlyphs, sizeof( int ) * xSize * ySize ); 

	for( int x = 0; x < xSize; x++ ) {
		for( int y = 0; y < ySize; y++ ) {
			*( g_canvasGlyphs + x + y * xSize ) = 0;
		}
	}

	g_canvasColors = realloc( g_canvasColors, sizeof( Color ) * xSize * ySize ); 

	for( int x = 0; x < xSize; x++ ) {
		for( int y = 0; y < ySize; y++ ) {
			*( g_canvasColors + x + y * xSize ) = ( Color ) { 0, 0, 0, 255 };
		}
	}

	g_canvasGlyphWidth = xSize;	
	g_canvasGlyphHeight = ySize;	

	g_canvasPixelWidth = xSize * GLYPHWIDTH;
	g_canvasPixelHeight = ySize * GLYPHHEIGHT;

	ui_scale_background();
}

void canvas_del()
{
	free( g_canvasGlyphs );
	free( g_canvasColors );

	g_canvasGlyphWidth = 0;
	g_canvasGlyphHeight = 0;

	g_canvasPixelWidth = 0;
	g_canvasPixelHeight = 0;
}

void canvas_save()
{
	char *data = malloc( sizeof( char ) );
	*data = '\0';

	canvas_append_string( &data, "SAPPFF" );
	canvas_append_string( &data, FILEFORMATVERSION );
	canvas_append_string( &data, "@");

	char xSize[ 4 ];
	sprintf( xSize, "%X", g_canvasGlyphWidth );
	canvas_append_string( &data, xSize );
	canvas_append_string( &data, "x");
	char ySize[ 4 ];
	sprintf( ySize, "%X", g_canvasGlyphHeight );
	canvas_append_string( &data, ySize );

	canvas_append_string( &data, "@");
	char bgColor[9];
	Color backgroundColor = ui_get_background_color();
	sprintf( bgColor, "%02X", backgroundColor.r );
	sprintf( bgColor + 2, "%02X", backgroundColor.g );
	sprintf( bgColor + 4, "%02X", backgroundColor.b );
	sprintf( bgColor + 6, "%02X", backgroundColor.a );
	canvas_append_string( &data, bgColor );
	
	for( int y = 0; y < g_canvasGlyphHeight; y++ ) {
		for( int x = 0; x < g_canvasGlyphWidth; x++ ) {
			canvas_append_string( &data, ";");

			const int glyphNbr = *( g_canvasGlyphs + x + y * g_canvasGlyphWidth );
			char nbrChar[ 4 ];
			sprintf( nbrChar, "%X", glyphNbr );
			canvas_append_string( &data, nbrChar );
			canvas_append_string( &data, ",");

			char colorHex[ 9 ];
			sprintf( colorHex, "%02X", ( *( g_canvasColors + x + y * g_canvasGlyphWidth ) ).r );
			sprintf( colorHex + 2, "%02X", ( *( g_canvasColors + x + y * g_canvasGlyphWidth ) ).g );
			sprintf( colorHex + 4, "%02X", ( *( g_canvasColors + x + y * g_canvasGlyphWidth ) ).b );
			sprintf( colorHex + 6, "%02X", ( *( g_canvasColors + x + y * g_canvasGlyphWidth ) ).a );

			canvas_append_string( &data, colorHex );
		}
	}

	const char *fileName = canvas_get_filename( SAPP );
	SaveFileText( fileName, data );
	
	#if defined( WASM )
	emscripten_run_script( TextFormat( "saveFileFromMEMFSToDisk('%s','%s')", fileName, fileName ) );
	#endif

	free( data );
}

void canvas_load( const char *filePath )
{
	char *data = LoadFileText( filePath );

	int fileFormat = strncmp( data, "SAPPFF", 6 );
	if( fileFormat != 0 ) return;

	char *versionString = strtok( data + 6, "@" );
	int version = ( int ) strtol( versionString, NULL, 16 );
	if( version != 0 ) return;

	char *xSizeString = strtok( NULL, "x" );
	int xSize = ( int ) strtol( xSizeString, NULL, 16 );
	char *ySizeString = strtok( NULL, "@" );
	int ySize = ( int ) strtol( ySizeString, NULL, 16 );
	canvas_new( xSize, ySize );	

	char *bgColorString = strtok( NULL, ";" );
	/*
	For some reason emscripten doesn't like it when you feed strtol
	32bit values so we drop the last 8 bits which is the alpha channel
	alpha is stored in the save file but not currently used so right now 2020-06-23 it doesn't matter
	*/
	#if defined( WASM )
	bgColorString[ 6 ] = '\0';
	#endif

	int bgColorHex = ( int ) strtol( bgColorString, NULL, 16 );

	Color bgColor;
	#if defined( WASM )
	bgColor.a = 255;
	bgColor.b = bgColorHex & 0xFF;
	bgColor.g = ( bgColorHex >> 8 ) & 0xFF;
	bgColor.r = ( bgColorHex >> 16 ) & 0xFF;
	#else 
	bgColor.a = bgColorHex & 0xFF;
	bgColor.b = ( bgColorHex >> 8 ) & 0xFF;
	bgColor.g = ( bgColorHex >> 16 ) & 0xFF;
	bgColor.r = ( bgColorHex >> 24 ) & 0xFF;
	#endif

	ui_set_background_color( bgColor );

	int maxIndex = xSize * ySize;
	int index = 0;
	while( 1 ) {
		char *glyphNbrChar = strtok( NULL, "," );
		int glyphNbr = ( int ) strtol( glyphNbrChar, NULL, 16 );
		g_canvasGlyphs[ index ] = glyphNbr;

		char *colorChar = strtok( NULL, ";" );
		#if defined( WASM )
		colorChar[6] = '\0';
		#endif

		unsigned long color = ( unsigned long ) strtol( colorChar, NULL, 16 ); 

		#if defined( WASM )
		g_canvasColors[ index ].a = 255;
		g_canvasColors[ index ].b = color & 0xFF;
		g_canvasColors[ index ].g = ( color >> 8 ) & 0xFF;
		g_canvasColors[ index ].r = ( color >> 16 ) & 0xFF;
		#else
		g_canvasColors[ index ].a =  color & 0xFF;
		g_canvasColors[ index ].b = ( color >> 8 ) & 0xFF;
		g_canvasColors[ index ].g = ( color >> 16 ) & 0xFF;
		g_canvasColors[ index ].r = ( color >> 24 ) & 0xFF;
		#endif

		index++;
		if( index == maxIndex ) break;
	}
}

void canvas_export_html()
{
	char *data = malloc( sizeof( char ) );
	*data = '\0';

	canvas_append_string( &data, "<!DOCTYPE html><html><head><style>.sapp{white-space:pre;font-family:monospace;}</style></head><body style=background-color:#" );

	char bgColorHex[ 9 ];
	Color bgColor = ui_get_background_color();
	sprintf( bgColorHex, "%02X", bgColor.r );
	sprintf( bgColorHex + 2, "%02X", bgColor.g );
	sprintf( bgColorHex + 4, "%02X", bgColor.b ); 
	sprintf( bgColorHex + 6, "%02X", bgColor.a ); 
	canvas_append_string( &data, bgColorHex );
	canvas_append_string( &data, "><div class=sapp>" );

	for( int y = 0; y < g_canvasGlyphHeight; y++ ) {
		for( int x = 0; x < g_canvasGlyphWidth; x++ ) {
			const int glyphNbr = *( g_canvasGlyphs + x + y * g_canvasGlyphWidth );
			const char *glyphUnicode = unicode_lookup_table( glyphNbr );

			char colorHex[ 9 ];
			sprintf( colorHex, "%02X", ( *( g_canvasColors + x + y * g_canvasGlyphWidth ) ).r );
			sprintf( colorHex + 2, "%02X", ( *( g_canvasColors + x + y * g_canvasGlyphWidth ) ).g );
			sprintf( colorHex + 4, "%02X", ( *( g_canvasColors + x + y * g_canvasGlyphWidth ) ).b );
			sprintf( colorHex + 6, "%02X", ( *( g_canvasColors + x + y * g_canvasGlyphWidth ) ).a );

			canvas_append_string( &data, "<span style=\"color:#" );
			canvas_append_string( &data, colorHex );
			canvas_append_string( &data, ";\">" );
			canvas_append_string( &data, "&#x" );
			canvas_append_string( &data, glyphUnicode );
			canvas_append_string( &data, ";</span>" );
		}

		canvas_append_string( &data, "<br/>" );
	}

	canvas_append_string( &data, "</div></body></html>" );

	const char *fileName = canvas_get_filename( HTML );
	SaveFileText( fileName, data );

	#if defined( WASM )
	emscripten_run_script( TextFormat( "saveFileFromMEMFSToDisk('%s','%s')", fileName, fileName ) );
	#endif

	free( data );
}

void canvas_export_png()
{
	Image png = GenImageColor( g_canvasPixelWidth, g_canvasPixelHeight, ui_get_background_color() );

	int xPng = 0;
	int yPng = 0;
	for( int i = 0; i < g_canvasGlyphWidth * g_canvasGlyphHeight; i++ ) {
		const int glyph = g_canvasGlyphs[ i ];
		const Color color = g_canvasColors[ i ];

		ImageDraw( &png, font, ( Rectangle ) { glyph * GLYPHWIDTH, 0, GLYPHWIDTH, GLYPHHEIGHT }, ( Rectangle ) { xPng, yPng, GLYPHWIDTH, GLYPHHEIGHT}, color );
		
		if( i % g_canvasGlyphWidth == ( g_canvasGlyphWidth - 1 ) ) {
			xPng = 0;
			yPng += GLYPHHEIGHT;
		}
		else xPng += GLYPHWIDTH;
	}

	const char *fileName = canvas_get_filename( PNG );

	ExportImage( png, fileName );

	#if defined( WASM )
	emscripten_run_script( TextFormat( "saveFileFromMEMFSToDisk('%s','%s')", fileName, fileName ) );
	#endif

	UnloadImage( png );
}

void canvas_append_string( char **mainString, const char *toAppend )
{
	const long int mainStringLength = strlen( *mainString );	
	const long int toAppendLength = strlen( toAppend );

	*mainString = realloc( *mainString, sizeof( char ) * ( mainStringLength + toAppendLength + 1 ) );		
	strcat( *mainString, toAppend );
}

const char* canvas_get_filename( enum fileTypes fileType )
{
	static char fileName[100];
	time_t rawTime;
	struct tm *info;

	time( &rawTime );
	info = localtime( &rawTime );

	strftime( fileName, 100, "SAPP-%FT%H-%M-%S", info );

	switch( fileType ) {
		case HTML:
			strcat( fileName, ".html" );
			break;
		case SAPP:
			strcat( fileName, ".sapp" );
			break;
		case PNG:
			strcat( fileName, ".png" );
			break;
		default:
			break;
	}

	return fileName;
}
