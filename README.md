Simple ASCII Paint Program
=========
After needing an ASCII paint program and finding that there weren't a whole lot of free alternatives I decided to roll my own. Very small feature set so I could churn it out quickly, so it's really only useful for simple images.

Only font set available is the venerable [code page 437](https://en.wikipedia.org/wiki/Code_page_437)

It's available as a webapp [here](https://0r33.itch.io/sapp)

Build instructions
---------
Requires raylib version 4.2.

Run make then copy 437.png to the build directory.

You can build the WASM version with make PLATFORM=WEB

User Guide
---------
![](guide.png)
1. The canvas. This is the ASCII drawing. Click on a grid slot in it to place a glyph.
2. Glyph select. You can have two glyphs selected at once, one bound to left mouse button and one bound to the right one. Left and right click on a glyph to bind it to that button. A red square around a glyphs indicates it's bound to left mouse button and blue indicates right mouse button.
3. Color select. Use the color picker widget to change color or input it manually using the R: G: and B: boxes below it. The selected color is shown next to "Color:"
NOTE: There's currently a bug in Raygui that affects the color picker. If you drag the little square in the gradient box all the way to the left then the vertical color slider gets stuck. The default color is black which means the gradient box has a value selected that's all the way to the left which in turn means that the color slider is stuck on startup. So before you can move the color slider you need to click somewhere in the gradient box to deselect black and get the slider unstuck.  I've filed a bug with Raygui and I'll update this project once it's fixed.
4. Replace options. Click on "Glyph" to put it in glyph replace mode, indicated by a green border around the replace button. Now if you left or right click on any glyph on the canvas all occurrences of it will be replaced with the glyph that was bound to the button you clicked with.
Click on "Color" to put it in color replace mode. Click on any glyph on the canvas and all occurrences of that glyph's color will be replaced with the currently selected color.
Note that replace modes are exited once they are used.
5. Set background color. Sets the background color to the one selected or clears it to transparent.
6. New. Clears and creates a new canvas to draw on. X and Y are it's dimensions in glyphs.
7. Clear Background Image. You can drag and drop a PNG image onto the canvas if you want to trace something. This button clears that image.
8. Save. Saves the image to a .sapp file in the present working directory or starts a download if you're using the WASM version. File handling is a bit crude, I should implement a file browser dialog for the desktop version. You can drag and drop these files onto the canvas to load them.
9. Export to HTML. Saves a HTML page with the ASCII. Also uses PWD.
10. Export to PNG. Also uses PWD.

Note on HTML export
---------
When exporting to HTML some of the more obscure glyphs will not render correctly because most fonts don't have the complete cp437 glyph set. So you need add a cp437 font file to the html that firstly has to include the cp437 glyphs but also have them mapped to their correct Unicode code points.

So for example these fonts will works: <https://int10h.org/oldschool-pc-fonts/fontlist/>

But these will not: <https://github.com/berenddeschouwer/fourthreeseven/>

Because even though it has all the glyphs they aren't mapped to the correct Unicode values.

If you just stick with the regular glyphs like letters, numbers and common signs then any mono-spaced font will work.

LICENSE
---------
Raygui is zlib license.

Everything else is MIT license.
