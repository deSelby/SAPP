PLATFORM ?= DESKTOP
	
SRC = $(wildcard ./src/*.c)
OBJ = $(patsubst ./src/%.c, %.o, $(SRC))
IDIR = ./src/
CFLAGS = -I$(IDIR) -I/usr/local/include/ -std=c99 -Wall -pedantic -g

ifeq ($(PLATFORM),DESKTOP)
	CC = gcc
	CFLAGS += -g
	LDFLAGS = -lraylib -lm -lpthread -ldl -lrt -lX11 -DPLATFORM_DESKTOP
endif
ifeq ($(PLATFORM),WEB)
	CC = emcc 
	EMCCFLAGS = -O2 -s USE_GLFW=3 -s ASSERTIONS=1 -s FORCE_FILESYSTEM=1 -s WASM=1 -s ALLOW_MEMORY_GROWTH=1 --preload-file 437.png --shell-file ./shell.html
	CFLAGS += -DWASM
	LDFLAGS = /usr/local/lib/libraylib.bc
	EXT = .html
endif

%.o: ./src/%.c
	$(CC) -c $< -o $@ $(CFLAGS) 

sapp: $(OBJ) $(SQLOBJ) 
	$(CC) $(OBJ) -o build/$@$(EXT) $(LDFLAGS) $(EMCCFLAGS)

clean:
	rm build/sapp* $(OBJ)
